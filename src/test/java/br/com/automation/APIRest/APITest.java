package br.com.automation.APIRest;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.approvaltests.Approvals;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class APITest 

{       
    @BeforeClass
    public static void setUp(){
        RestAssured.baseURI = "https://postman-echo.com";
    }

    @Test
    public void Get()
    {               
        given()
            .relaxedHTTPSValidation()
            .param("foo1", "bar1")
            .param("foo2", "bar2")
        .when()
            .get("/get")
        .then()
            .statusCode(200) // O status http retornado foi 200
            .contentType(ContentType.JSON) // O response foi retornado no formato JSON
            .body("headers.host", is("postman-echo.com")) // A chave “host” possui exatamente o valor “postman-echo.com”
            .body("args.foo1", containsString("bar")); //A chave “foo1” contém o valor “bar”
    }

    @Test
    public void Post()
    {            
        given()
            .relaxedHTTPSValidation()
            .header("Content-Type", "application/json")            
            .body("{\n" +
                            "   \"name\": \"Rest Assured\", \n" + 
                            "   \"description\": \"Framework to test API Rest\"\n" +
                            "}")
        .when().log().all()
            .post("/post")
            
        .then().log().all()
            .statusCode(200) // O status http retornado foi 200
            .contentType(ContentType.JSON) // O response foi retornado no formato JSON
            .body("data.name", equalTo("Rest Assured")) // A chave "name" possui exatamente o valor "Rest Assured"
            .body("data.description", equalTo("Framework to test API Rest")); // A chave "description" possui exatamente o valor "Framework to test API Rest"
    }

    @Test
	public void postJsonObjectExample() throws JSONException {
		JSONObject request = new JSONObject();
		request.put("name","Rest Assured DEMO - TEXT Test Demo 2");
		request.put("description","Rest Assured DEMO");
		request.put("type","TEXT");

		given()
                .relaxedHTTPSValidation()
                .header("Content-Type", "application/json") 
				.body(request.toString())
		.when().log().all()
				.post("/post")
		.then().log().all()
				.statusCode(200)
				.body("data.name", equalTo("Rest Assured DEMO - TEXT Test Demo 2"))
				.body("data.description", equalTo("Rest Assured DEMO"))
				.body("data.type", equalTo("TEXT"));

    }
    
    @Test
	public void postExampleWithReadJson() throws IOException {

		FileInputStream file = new FileInputStream("src/test/java/br/com/automation/APIRest/resources/createAttributePostExample.json");
        String data = IOUtils.toString(file, StandardCharsets.UTF_8);
        
		given()
                .relaxedHTTPSValidation()
                .header("Content-Type", "application/json")                 
				.body(data)
		.when().log().all()
				.post("/post")
		.then().log().all()
				.statusCode(200)
				.body("data.name", equalTo("Rest Assured DEMO - File 1"))
				.body("data.description", equalTo("API Rest automation framework"))
				.body("data.metadata.valueType", equalTo("TEXT"))
				.body("data.metadata.values[0]", equalTo("test1"))
                .body("data.metadata.values[1]", equalTo("test2"))
                .body("data.metadata.values[2]", equalTo("test3"));
    }
    
    @Test
	public void postExampleWithAssert() throws IOException {

		FileInputStream file = new FileInputStream("src/test/java/br/com/automation/APIRest/resources/createAttributePostExample.json");
		String data = IOUtils.toString(file, StandardCharsets.UTF_8);

		String result =  given()
                .relaxedHTTPSValidation()
                .header("Content-Type", "application/json")                 
                .body(data)
		.when().log().all()
				.post("/post")
		.then().log().all()
				.statusCode(200).extract().path("data.name");

		System.out.println(result);

		 assertEquals(result, "Rest Assured DEMO - File 1");

    }
    
    @Test
	public void getExampleWithJsonValidator() {

		String result = given()
                .relaxedHTTPSValidation()
                .param("foo1", "bar1")
                .param("foo2", "bar2")                 
		.when().log().all()
				.get("https://reqres.in/api/users/2")
		.then().log().all()
				.statusCode(200).extract().asString();

		Approvals.verifyJson(result);
	}
}
